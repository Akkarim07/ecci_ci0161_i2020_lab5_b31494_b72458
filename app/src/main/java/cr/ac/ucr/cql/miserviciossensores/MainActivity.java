package cr.ac.ucr.cql.miserviciossensores;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;

import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.tasks.OnSuccessListener;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity implements
        DownloadCallback,
        SensorEventListener
       {
    // Compomentes de la interfaz
    private ProgressDialog mDialog;
    private WebView mWebView;
    private ImageView mImageView;
    private TextView mDataText;
    private BroadcastReceiver mReceiver;

    public static final String TAG_IMG = "IMG";
    private NetworkFragment mNetworkFragment;
    private boolean mDownloading = false;
    protected static final String TAG = "MainActivity";

    // Parametros para el request en FusedLocationProviderApi
    protected LocationRequest mLocationRequest;
    // Tipos de servicios de localizacion
    protected LocationSettingsRequest mLocationSettingsRequest;
    // Localizacion geografica
    protected Location mCurrentLocation;
    // Estatus del request
    protected boolean mRequestingLocationUpdates;
    // Tiempo de reqgistro de la localizacion
    protected String mLastUpdateTime;

    LocationRequest locationRequest;
    LocationCallback locationCallback;
    FusedLocationProviderClient fusedLocationProviderClient;

    //intervalo de chequeo de localizacion
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 5000; //10000
    // tasa limite para las actualizaciones, no mas ferecuentes que este valor
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    public static final int PERMISSION_REQUEST = 666;

    // Servicio acelerómetro
    private long ultimaActualizacion = 0;
    private float previoX = 0, previoY = 0, previoZ = 0;
    private float actualX = 0, actualY = 0, actualZ = 0;

    // Servicios SOAP
    private static final String URL = "http://wsgeoip.lavasoft.com/ipservice.asmx";
    private static final String NAMESPACE = "http://wsgeoip.lavasoft.com/";
    private static final String METHOD_NAME = "GetIpLocation";
    private static final String SOAP_ACTION = "http://lavasoft.com/GetIpLocation";

    //Servicios REST
    private static final String URL_REST = "https://restcountries.eu/rest/v2/alpha/CRI";

    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mWebView = (WebView) findViewById(R.id.webView);
        mWebView.setVisibility(View.INVISIBLE);

        mImageView = (ImageView) findViewById(R.id.imageView);
        mImageView.setVisibility(View.INVISIBLE);

        mDataText = (TextView) findViewById(R.id.text);
        mDataText.setVisibility(View.INVISIBLE);

        // servicios de localizacion
        mRequestingLocationUpdates = false;
        mLastUpdateTime = "";

        // Creamos los objetos LocationRequest, LocationSettingsRequest
        createLocationRequest();
        buildLocationCallBack();

        // Servicio BroadcastReceiver
        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String s = intent.getStringExtra(ServicioSimple.SERVICE_MESSAGE);
                String txt = mDataText.getText().toString();
                txt += s + "\n";
                mDataText.setText(txt);
            }
        };

        handler = new Handler(){
            @Override
            public void handleMessage(Message msg){
                super.handleMessage(msg);
                String txt = (String) msg.obj;
                mDataText.setText(mDataText.getText() + "\n" + txt);
            }
        };
    }

    // Menu de opciones
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.example_one:
                // Mostramos un dialogo de barra de progreso mediante una tarea
                ejemploBarraProgreso();
                return true;

            case R.id.example_two:
                // Mostramos una vista web mediante una tarea
                ejemploVistaWeb();
                return true;

            case R.id.example_three:
                // Mostramos una imagen mediante una tarea
                ejemploMostrarImagen();
                return true;

            case R.id.example_four_init_action:
                // Iniciamos el fragmento que ejecutamos en la tarea asincronica pasando el URL que buscamos su contenido
                startFragmentTask("https://www.facebook.com");
                return true;

            case R.id.example_four_fetch_action:
                // Cuando el usuario hace clic en extraer, se obtiene los primeros N caracteres del HTML
                startDownload();
                return true;

            case R.id.example_four_clear_action: //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-
                // Limpiamos el texto de resultado y cancelamos el download si este aun esta en proceso
                finishDownloading();
                mDataText.setText("Cierre de tarea.");
                return true;

            case R.id.example_five_init_action:
                // Iniciamos el fragmento que ejecutamos en la tarea asincronica pasando el URL que buscamos
                // Este devuele un html a partir del servicion REST de Google
                startFragmentTask("https://www.google.com/search?q=universidad+costa+rica");
                return true;

            case R.id.example_five_fetch_action:
                // Cuando el usuario hace clic en extraer, se obtiene los primeros N caracteres del HTML
                // Iniciamos el download de la página
                startDownload();
                return true;

            case R.id.example_five_clear_action:
                // Limpiamos el texto de resultado y cancelamos el download si este aun esta en proceso
                finishDownloading();
                mDataText.setText("Cierre de tarea.");
                return true;

            case R.id.example_six_init_action:
                // Iniciamos el servicio
                inicioServicio();
                return true;

             case R.id.example_six_clear_action:
             // Detenemos el servicio
                 finServicio();
                return true;

            case R.id.example_seven_connect_action:
                // Servicio de localizacion
                conectarServicioLocalizacion();
                return true;

            case R.id.example_seven_init_action:
                // Servicio de localizacion
                iniciarServicioLocalizacion();
                return true;

            case R.id.example_seven_clear_action:
                // Detener el servicion de localizacion
                stopLocationUpdates();
                return true;

            case R.id.example_seven_disconnect_action:
                // Servicion de localizacion
                desconectarServicioLocalizacion();
                return true;

            case R.id.example_eigth_connect_action:
                // Servicion de acelerómetro
                conectarServicioAcelerometro();
                return true;

            case R.id.example_eigth_disconnect_action:
                 // Servicion de acelerómetro
                 desconectarServicioAcelerometro();
                 return true;

            case R.id.example_nine_init_action:
                servicioSOAP();
                return true;

            case R.id.example_ten_init_action:
                servicioREST();
                return true;

        }
        return false;
    }

    private void ejemploBarraProgreso() {
        // Mostramos un dialogo de barra de progreso
        mDialog = new ProgressDialog(this);
        mDialog.setMessage("Ejecutando tarea asincrónica...");
        mDialog.setTitle("Progreso");
        mDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mDialog.setCancelable(false);
        mDialog.setMax(100);
        mDialog.show();
        // Ejecutamos la tarea asincronica y actualizamos la barra de progreso
        new TaskBarraProgreso().execute();
    }

    private void ejemploVistaWeb() {
        // Solo visible el web view
        mWebView.setVisibility(View.VISIBLE);
        // configuro que el webview se abra dentro de la app
        mWebView.setWebViewClient(new WebViewClient());
        // opciones de configuracion del webview
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setLoadsImagesAutomatically(true);
        // Ejecutamos la tarea asincronica para mostrar el contenido de una pagina web
        // mediante un Runnable que permite la ejecucion de tareas ejecutadas por un thread
        // metodo HTTP como POST
        mWebView.post(new Runnable() {
            @Override
            public void run() {
                mWebView.loadUrl("http://www.google.com");
            }
        });
    }

    private void ejemploMostrarImagen() {
        // Solo visible la imagen
        mWebView.setVisibility(View.INVISIBLE);
        mImageView.setVisibility(View.VISIBLE);
        // Ejecutamos la tarea asincronica que baja y muestra la imagen con la ruta de descarga
        new
                TaskMostrarImagen().execute("https://www.ecci.ucr.ac.cr/sites/default/files/ecci-escuela-ciencias-computacion-informatica-horizontal_0.png");
    }

    // Servicio Http download
    private void startFragmentTask(String url) {
        // Solo visible el text view
        mDataText.setVisibility(View.VISIBLE);
        mWebView.setVisibility(View.INVISIBLE);
        mImageView.setVisibility(View.INVISIBLE);
        mDataText.setText(url);
        // Iniciamos el fragmento que ejecutamos en la tarea asincronica pasando el URL que buscamos su contenido
        mNetworkFragment = NetworkFragment.getInstance(getSupportFragmentManager(), url);
    }

    private void startDownload() {
        // si no estamos bajando contenido y el fragmento con la tarea fue inicializado
        if (!mDownloading && mNetworkFragment != null) {
            // Ejecutamos el download en la tarea async
            mNetworkFragment.startDownload();
            // Marcamos el download en proceso
            mDownloading = true;
        }
    }

    private void inicioServicio() {
        // Solo visible el text view
        mDataText.setVisibility(View.VISIBLE);
        mWebView.setVisibility(View.INVISIBLE);
        mImageView.setVisibility(View.INVISIBLE);
        mDataText.setText("");
        // Configuro para recibir mensajes desde el servicio
        LocalBroadcastManager.getInstance(this).registerReceiver((mReceiver),
                new IntentFilter(ServicioSimple.SERVICE_RESULT) );
        // Inicio del servicio Intent
        Intent serviceIntent = new Intent(this, ServicioSimple.class);
        serviceIntent.addCategory(ServicioSimple.SERVICE_TAG); startService(serviceIntent);
    }

    private void finServicio() {
        // Detener del servicio Intent
        Intent serviceIntent = new Intent(this, ServicioSimple.class);
        serviceIntent.addCategory(ServicioSimple.SERVICE_TAG);
        stopService(serviceIntent);
        // Detengo recibir mensajes desde el servicio
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
    }

    @Override
    public void finishDownloading() {
        // marcamos que finaliza el proceso de download
        mDownloading = false;
        // Cancelamos el fragmento con la tarea async
        if (mNetworkFragment != null) {
            mNetworkFragment.cancelDownload();
        }
        NetworkFragment networkFragment = (NetworkFragment) getSupportFragmentManager()
                .findFragmentByTag(NetworkFragment.TAG_FRAGMENT);
        if (networkFragment != null) {
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().remove(networkFragment).commit();
            manager.popBackStack();
        }
    }

    // Actualizamos y mostramos los datos resultado del download
    @Override
    public void updateFromDownload(String result) {
        if (result != null) {
            mDataText.setText(result);
        Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
        } else {
            mDataText.setText(result);
            Toast.makeText(getApplicationContext(), "Error de Conexion", Toast.LENGTH_SHORT).show();
        }
    }

    // Este metodo permite mostrar el estado de la tarea Async download
    @Override
    public void onProgressUpdate(int progressCode, int percentComplete) {
        switch (progressCode) {
            // You can add UI behavior for progress updates here.
            case Progress.ERROR:
                Toast.makeText(getApplicationContext(), "ERROR", Toast.LENGTH_SHORT).show();
                break;
            case Progress.CONNECT_SUCCESS:
                Toast.makeText(getApplicationContext(), "CONNECT_SUCCESS", Toast.LENGTH_SHORT).
                        show();
                break;
            case Progress.GET_INPUT_STREAM_SUCCESS:
                Toast.makeText(getApplicationContext(), "GET_INPUT_STREAM_SUCCESS",
                        Toast.LENGTH_SHORT).show();
                break;
            case Progress.PROCESS_INPUT_STREAM_IN_PROGRESS:
                Toast.makeText(getApplicationContext(), "PROCESS_INPUT_STREAM_IN_PROGRESS",
                        Toast.LENGTH_SHORT).show();
                break;

            case Progress.PROCESS_INPUT_STREAM_SUCCESS:
                Toast.makeText(getApplicationContext(),
                        "PROCESS_INPUT_STREAM_SUCCESS", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public NetworkInfo getActiveNetworkInfo() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo;
    }

    // Estado de la red, verificar el estado de la red
    public boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    //Creamos el request de localizaciones
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(500)
                .setFastestInterval(0)
                .setMaxWaitTime(0)
                .setSmallestDisplacement(0)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /*Se dejan por completitud del lab, pero no hacen nada porque está deprecated.*/
    private void conectarServicioLocalizacion() {
            Toast.makeText(this, "mGoogleApiClient conectado", Toast.LENGTH_SHORT).show();

    }

    /*Se dejan por completitud del lab, pero no hacen nada porque está deprecated.*/
    private void desconectarServicioLocalizacion() {
            Toast.makeText(this, "mGoogleApiClient desconectado", Toast.LENGTH_SHORT).show();

    }

    // Servicios de localizacion
    protected void startLocationUpdates() {
        mDataText.setText("Inicio GPS:\n");
        // Antes de solicitar la localizacion tenemos que asegurar que tenemos permisos
        if (!checkPermission()) {
            requestPermission();
            return;
        }
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        fusedLocationProviderClient.getLastLocation()
            .addOnSuccessListener(new OnSuccessListener<Location>() {
                  @Override
                  public void onSuccess(Location location) {
                      if (location != null) {
                          mCurrentLocation = location;
                          mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                          updateUI();
                      }
                  }
              });

        fusedLocationProviderClient.requestLocationUpdates(mLocationRequest, locationCallback, null);
    }

    // Servicios de localizacion
    private void updateUI() {
        if (mCurrentLocation != null) {
            // contatenamos los datos de localizacion recibidos
            String txt = mDataText.getText().toString();
            txt += " /*/Servicio de Localización/*/ \n" +
                    "Lat: " + String.valueOf(mCurrentLocation.getLatitude()) + "\n" +
                    "Lon: " + String.valueOf(mCurrentLocation.getLongitude()) + "\n" +
                    "Time: " + mLastUpdateTime + "\n";
            mDataText.setText(txt);
        }
    }

    // Servicios de localizacion
    // si la actividad pausa debe detener el servicio de localizacion
    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    // Servicios de localizacion
    @Override
    protected void onStop() {
        super.onStop();
        stopLocationUpdates();
    }

    // Servicios de localizacion
    public void iniciarServicioLocalizacion() {
        // Solo visible el text view
        mDataText.setVisibility(View.VISIBLE);
        mWebView.setVisibility(View.INVISIBLE);
        mImageView.setVisibility(View.INVISIBLE);
        // Iniciamos el servicio de localizacion
        if (!mRequestingLocationUpdates) {
            mRequestingLocationUpdates = true;
            startLocationUpdates(); //
            Toast.makeText(this, "Inicio de servicios de localizacion",
                    Toast.LENGTH_SHORT).show();
        }
    }

    // Servicios de localizacion
    protected void stopLocationUpdates() {
        try {
            if (locationCallback != null) {
                fusedLocationProviderClient.removeLocationUpdates(locationCallback);
                Toast.makeText(getApplicationContext(), "Finalización de servicios de localización", Toast.LENGTH_SHORT).show();
            }
        }catch(Exception e){
            Log.d("exception", e.toString());
        }
        mRequestingLocationUpdates = false;
    }

    private void conectarServicioAcelerometro(){
        mDataText.setVisibility(View.VISIBLE);
        mWebView.setVisibility(View.INVISIBLE);
        mImageView.setVisibility(View.INVISIBLE);

        SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);

        List<Sensor> sensors = sm.getSensorList(Sensor.TYPE_ACCELEROMETER);

        if(sensors.size() > 0){
            sm.registerListener(this, sensors.get(0), SensorManager.SENSOR_DELAY_UI);
        }
    }

    private void desconectarServicioAcelerometro(){
        SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        sm.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        synchronized (this){
            long mTimeStamp = event.timestamp;

            actualX = event.values[0];
            actualY = event.values[1];
            actualZ = event.values[2];

            if(previoX == 0 && previoY == 0 && previoZ == 0){
                ultimaActualizacion = mTimeStamp;
                previoX = actualX;
                previoY = actualY;
                previoZ = actualZ;
            }

            long timeDifference = mTimeStamp - ultimaActualizacion;
            if(timeDifference > 1000000000L){
                float movementX = Math.abs(actualX - previoX);
                float movementY = Math.abs(actualY - previoY);
                float movementZ = Math.abs(actualZ - previoZ);
                float minMovement = 1.5f;

                if(movementX > minMovement){
                    String txt = mDataText.getText().toString();
                    txt += "\n /*/Hay Movimiento X/*/ \n\n";
                    mDataText.setText(txt);
                }

                if(movementY > minMovement){
                    String txt = mDataText.getText().toString();
                    txt += "\n /*/Hay Movimiento Y/*/ \n\n";
                    mDataText.setText(txt);
                }

                if(movementZ > minMovement*3){
                    String txt = mDataText.getText().toString();
                    txt += "\n /*/Hay Movimiento Z/*/ \n\n";
                    mDataText.setText(txt);
                }

                previoX = actualX;
                previoY = actualY;
                previoZ = actualZ;
                ultimaActualizacion = mTimeStamp;

                String txt = mDataText.getText().toString();
                txt += " /*/Servicio de Acelerómetro/*/ \n" +
                        "X: " + actualX + "\n" +
                        "Y: " + actualY + "\n" +
                        "Z: " + actualZ + "\n";
                mDataText.setText(txt);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private void servicioSOAP(){
        mDataText.setVisibility(View.VISIBLE);
        mWebView.setVisibility(View.INVISIBLE);
        mImageView.setVisibility(View.INVISIBLE);
        mDataText.setText("/*/ Llamada a web service SOAP /*/ \n");

        Thread mJob = new Thread(){
            @Override
            public void run(){
                String resultValue = "\n GetGeoIP: " + "74.125.91.106" + "\n";
                try{
                    ResultadoGeoIPService mGeoIPService = GetGeoIP("74.125.91.106", NAMESPACE, METHOD_NAME, SOAP_ACTION, URL);
                    resultValue = resultValue + "\n \n" + mGeoIPService.toString();
                }catch(Exception e){
                    resultValue = resultValue + "\n \n" + "ERROR: " + e.getMessage();
                }
                Message msg = handler.obtainMessage();
                msg.obj = resultValue;
                handler.sendMessage(msg);
            }
        };
        mJob.start();

    }

    public ResultadoGeoIPService GetGeoIP(String mIP, String mNamespace, String mMetodo, String mAction, String mUrl) throws Exception{
        try {
            SoapObject mRequest = new SoapObject(mNamespace, mMetodo);
            PropertyInfo p = new PropertyInfo();
            p.setName("GetIpAddress");
            p.setValue(mIP);
            p.setType(String.class);
            mRequest.addProperty(p);

            SoapSerializationEnvelope mEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            mEnvelope.setOutputSoapObject(mRequest);
            mEnvelope.dotNet = true;
            HttpTransportSE mTransporte = new HttpTransportSE(mUrl);
            mTransporte.debug = true;
            mTransporte.call(mAction, mEnvelope);
            SoapObject resultado = (SoapObject) mEnvelope.bodyIn;

            ResultadoGeoIPService mResultadoGeoIPService = new ResultadoGeoIPService();
            mResultadoGeoIPService.setNombrePais(resultado.getProperty("GetIpLocationResult").toString());

            return mResultadoGeoIPService;
        }catch(Exception e){
            throw(e);
        }
    }

    private void servicioREST(){
        mDataText.setVisibility(View.VISIBLE);
        mWebView.setVisibility(View.INVISIBLE);
        mImageView.setVisibility(View.INVISIBLE);

        mDataText.setText("/*/ Llamada a web service REST /*/ \n");

        mDataText.setText(URL_REST);

        new TaskServicioREST().execute(URL_REST);
    }

    private class TaskServicioREST extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... urls) {
            return loadContentFromNetwork(urls[0]);
        }

        protected void onPostExecute(String result){
            mDataText.setText(mDataText.getText() + "\n \n" + result);
        }

        private String loadContentFromNetwork(String url){
            try {
                InputStream mInputStream = (InputStream) new URL(url).getContent();
                InputStreamReader minputStreamReader = new InputStreamReader(mInputStream);
                BufferedReader responseBuffer = new BufferedReader(minputStreamReader);
                StringBuilder stringBuilder = new StringBuilder();
                String line = null;
                while((line = responseBuffer.readLine()) != null){
                    stringBuilder.append(line);
                }
                return stringBuilder.toString();
            }catch(Exception e){
                Log.v(TAG_IMG, e.getMessage());
            }
            return null;
        }
    }

           //********************************************************************************************** //
//Clase para la tarea asincronica que muestra la barra de progreso
// Params -> String -> doInBackground
// Progress -> Float -> onProgressUpdate
// Result -> Integer -> onPostExecute
            private class TaskBarraProgreso extends AsyncTask<String, Float, Integer> {

                @Override
                protected void onPreExecute() {
                    // antes de ejecutar
                    mDialog.setProgress(0);
                }

                @Override
                protected Integer doInBackground(String... params) {
                    // Cuando se dispara la tarea simulamos el procesamiento en la tarea asincrnica
                    for (int i = 0; i < 100; i++) {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) { }
                        // publicamos el progreso de la tarea
                        publishProgress(i/100f);
                    }
                    return 100;
                }

                protected void onProgressUpdate (Float... values) {
                    // actualizamos la barra de progreso y sus valores
                    int p = Math.round(100 * values[0]);
                    mDialog.setProgress(p);
                }

                protected void onPostExecute(Integer bytes) {
                    // Cuando la tarea termina cerramos la barra de progreso
                    mDialog.dismiss();
                }
            }

            //**********************************************************************************************//
            // Clase para la tarea asincronica de Imagen
            private class TaskMostrarImagen extends AsyncTask<String, Void, Bitmap> {
                // La tarea se ejecuta en un thread tomando como parametro el eviado en
                // AsyncTask.execute()
                @Override
                protected Bitmap doInBackground(String... urls) {
                    // tomanos el parámetro del execute() y bajamos la imagen
                    return loadImageFromNetwork(urls[0]);
                }
                // El resultado de la tarea tiene el archivo de imagen el cual mostramos
                protected void onPostExecute(Bitmap result) {
                    mImageView.setImageBitmap(result);
                }
                // metodo para bajar la imagen
                private Bitmap loadImageFromNetwork(String url) {
                    try {
                        Bitmap bitmap = BitmapFactory.decodeStream( (InputStream) new URL(url).getContent());
                        return bitmap;
                    } catch (Exception e) {
                        Log.v(TAG_IMG, e.getMessage());
                    }
                    return null;
                }
            }

           private void buildLocationCallBack() {
               locationCallback = new LocationCallback()
               {
                   @Override
                   public void onLocationResult(LocationResult locationResult) {
                       for(Location location :locationResult.getLocations())
                       {
                           mCurrentLocation = location;
                           mLastUpdateTime = DateFormat.getDateTimeInstance().format(new Date());
                           updateUI();
                       }
                   }
               };
           }

           private void requestPermission(){
               ActivityCompat.requestPermissions(this,
                       new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                       PERMISSION_REQUEST);
           }

           private boolean checkPermission()
           {
               return ActivityCompat.checkSelfPermission(this,
                       Manifest.permission.ACCESS_FINE_LOCATION) ==
                       PackageManager.PERMISSION_GRANTED &&
                       ActivityCompat.checkSelfPermission(this,
                               Manifest.permission.ACCESS_COARSE_LOCATION)
                               == PackageManager.PERMISSION_GRANTED;
           }

           @Override
           public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
               switch (requestCode) {
                   case PERMISSION_REQUEST: {
                       // If request is cancelled, the result arrays are empty.
                       if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                           startLocationUpdates();
                       }
                       return;
                   }
               }
           }
        }

